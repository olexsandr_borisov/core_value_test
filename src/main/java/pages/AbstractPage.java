package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.internal.WrapsElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static configs.TestConfig.getDriver;

public abstract class AbstractPage {
    private static final long DEFAULT_TIMEOUT_TO_WAIT = 20;

    public AbstractPage() {
        PageFactory.initElements(getDriver(), this);
        waitUntilLoaded();
    }

    public void waitForVisibilityOf(WebElement element) {
        waitForVisibilityOf(element, DEFAULT_TIMEOUT_TO_WAIT);
    }

    public void waitForVisibilityOf(WebElement element, long timeoutInSeconds) {
        webDriverWait(element, timeoutInSeconds).until(ExpectedConditions.visibilityOf(element));
    }

    public void scrollIntoViewByCoordinate(WebElement element) {
        getJavascriptExecutor(element).executeScript("arguments[0].scrollIntoView(true); scrollBy(0,-300);", element);
    }

    public void slide(WebElement element, int x, int y) {
        Actions actions = new Actions(getDriver());
        actions.clickAndHold(element).moveByOffset(x, y).release();
        actions.perform();
    }

    private static WebDriver getWebDriverFromSearchContext(SearchContext searchContext) {
        if (searchContext instanceof WebDriver) {
            return (WebDriver) searchContext;
        } else if (searchContext instanceof WrapsDriver) {
            return getWebDriverFromSearchContext(((WrapsDriver) searchContext).getWrappedDriver());
        } else {
            return searchContext instanceof WrapsElement ? getWebDriverFromSearchContext(((WrapsElement) searchContext).getWrappedElement()) : null;
        }
    }

    private WebDriverWait webDriverWait(WebElement element, long timeInSeconds) {
        WebDriverWait waiter = new WebDriverWait(getWebDriverFromSearchContext(element), timeInSeconds);
        waiter.ignoring(StaleElementReferenceException.class);
        return waiter;
    }

    private JavascriptExecutor getJavascriptExecutor(WebElement element) {
        return (JavascriptExecutor) getWebDriverFromSearchContext(element);
    }

    protected void waitUntilLoaded(){
           
    }
}