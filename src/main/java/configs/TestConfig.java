package configs;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

public class TestConfig {
    public static final int DEFAULT_TIMEOUT_TO_WAIT = 20;
    public static final int DEFAULT_PAGE_LOAD_TIME = 30;
    public static final String ROOT_URL = "http://ia.ca/";

    private static final ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    @Parameters({"browser"})
    @BeforeTest
    public void setUp(@Optional("chrome") String browser, final ITestContext context) {

        if (browser.equals("chrome")) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            WebDriverManager.chromedriver().setup();
            driver.set(new ChromeDriver(options));
            System.out.println("Starting " + driver.get().getClass().getSimpleName() + "..."
                    + WebDriverManager.chromedriver().getDownloadedVersion());
        }

        if (browser.equals("firefox")) {
            FirefoxOptions options = new FirefoxOptions();
            // More info on: https://developers.google.com/web/updates/2017/04/headless-chrome
            options.addArguments("-headless");
            driver.set(new FirefoxDriver(options));
            System.out.println("Starting " + driver.get().getClass().getSimpleName() + "..."
                    + WebDriverManager.firefoxdriver().getDownloadedVersion());
        }

        driver.get().manage().window().maximize();
        driver.get().manage().timeouts().pageLoadTimeout(DEFAULT_PAGE_LOAD_TIME, TimeUnit.SECONDS);
        driver.get().manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT_TO_WAIT, TimeUnit.SECONDS);
    }

    @AfterTest(alwaysRun = true)
    public void BrowserQuit() {
        System.out.println("Closing " + driver.get().getClass().getSimpleName() + "...");
        if (driver.get() != null) {
            driver.get().quit();
        }
    }

    public static WebDriver getDriver() {
        return driver.get();
    }
}
