package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

public class CalculatorPage extends AbstractPage {
    @FindBy(css = "#form_calculateur_versements > div:nth-child(2) > div > div.slider-box.full-width > div > div.slider-track > div.slider-handle.min-slider-handle.custom")
    private WebElement purchasePriceSlider;
    @FindBy(id = "PrixPropriete")
    private WebElement purchasePriceInput;
    @FindBy(id = "MiseDeFond")
    private WebElement downPaymentInput;
    @FindBy(id = "PrixProprietePlus")
    private WebElement buttonPrixProprietePlus;
    @FindBy(id = "MiseDeFondPlus")
    private WebElement buttonDownPaymentPlus;
    @FindBy(css = "#form_calculateur_versements > div:nth-child(4) > div:nth-child(1) > div > div.selectric > b")
    private WebElement buttonDropDownAmortizationYears;
    @FindBy(css = "#form_calculateur_versements > div:nth-child(4) > div:nth-child(1) > div > div.selectric-items > div > ul > li[data-index=\"0\"]")
    private WebElement optionAmortizationYear15;
    @FindBy(css =  "#form_calculateur_versements > div.selectric-wrapper.selectric-responsive > div.selectric > b")
    private WebElement buttonDropDownPaymentFrequency;
    @FindBy(css =  "#form_calculateur_versements > div.selectric-wrapper.selectric-responsive.selectric-open.selectric-hover > div.selectric-items > div > ul > li[data-index=\"3\"]")
    private WebElement optionPaymentFrequencyWeekly;
    @FindBy(id =  "TauxInteret")
    private WebElement inputSendTauxInteret;
    @FindBy(id =  "btn_calculer")
    private WebElement buttonCalculate;
    @FindBy(id =  "paiement-resultats")
    private WebElement textPaimentResult;


    @Step("Scroll to purchase price input")
    public void scrollToPurchase(){
        scrollIntoViewByCoordinate(purchasePriceInput);
    }

    public int getPurchasePrice() {
        return Integer.parseInt(purchasePriceInput.getAttribute("value"));
    }

    public int getDownPayment() {
        return Integer.parseInt(downPaymentInput.getAttribute("value"));
    }

    @Step("Move to purchase price input")
    public void movePurchaseSlider(int x, int y) {
        slide(purchasePriceSlider, x, y);
    }

    @Step("Click on frex propriete plus")
    public void clickOnPrixProprietePlusButton(){
        buttonPrixProprietePlus.click();
    }

    @Step("Click on down payment plus")
    public void clickOnDownPaymentPlusButton(){
        buttonDownPaymentPlus.click();
    }

    @Step("Click on DropDowm amortization years")
    public void clickOnDropDownAmortizationYears(){
        buttonDropDownAmortizationYears.click();
    }

    @Step("Choose amortization year - 150")
    public void chooseAmortizationYear15Option(){
        optionAmortizationYear15.click();
    }

    @Step("Click on DropDown payment frequency")
    public void clickOnDropDownPaymentFrequency(){
        buttonDropDownPaymentFrequency.click();
    }

    @Step("Choose payment frequency weekly option")
    public void choosePaymentFrequencyWeeklyOption(){
        optionPaymentFrequencyWeekly.click();
    }

    @Step("Send taux interet")
    public void sendTauxInteret(String value){
        inputSendTauxInteret.clear();
        inputSendTauxInteret.sendKeys(value);
    }

    @Step("Click on calculate button ")
    public void clickOnCalculateButton(){
        buttonCalculate.click();
    }

    @Step("Wait payment result")
    public void waitPaymentResult(){
        waitForVisibilityOf(textPaimentResult);
    }

    public String getPaymentResult(){
       return textPaimentResult.getText();
    }
}
