package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

public class MainPage extends AbstractPage {
    @Override
    protected void waitUntilLoaded() {

    }

    @FindBy(css = "#nav-secondaire > div.navbar-sub-main > ul > li.dropdown.Pret.three-items > a > span")
    private WebElement clickLOANS;

    @FindBy(css = "#nav-secondaire > div.navbar-sub-main > ul > li.dropdown.Pret.three-items.open > ul > li:nth-child(1) > section > ul > li:nth-child(1) > a")
    private WebElement openMortgage;

    //    @FindBy(css = "div a[data-utag-name=\"calculate_your_payments\"][data-utag-type=\"button\"]")
    @FindBy(css = "#grille-zone-capacite-versements > div > a.section-cliquable.section-cliquable-versements > div > h3")
    private WebElement buttonCalculateYourPayments;

    @FindBy(id = "PrixPropriete")
    private WebElement fieldPurchasePrice;

    @Step("Click LOANS")
    public MainPage clickOnLoans() {
        clickLOANS.click();
        return this;
    }

    @Step("Click the Mortgages link")
    public MainPage clickMortgage() {
        openMortgage.click();
        return this;
    }

    @Step("Click the Calculate Your Payments button")
    public MainPage clickOnCalculateYourPayments() {
        buttonCalculateYourPayments.click();
        return this;
    }

    public MainPage waitCalculatePage() {
        waitForVisibilityOf(fieldPurchasePrice);
        return this;
    }
}
