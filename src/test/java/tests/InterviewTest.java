package tests;

import pages.CalculatorPage;
import pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class InterviewTest extends AbstractWebTest {
    public MainPage mainPage;
    public pages.CalculatorPage calculatorPage;

    private static final int PURCHASE_PRICE = 500000;
    private static final int DOWN_PAYMENT = 50000;

    @BeforeClass
    public void before() {
        mainPage = new MainPage();
        calculatorPage = new CalculatorPage();
    }

    @Test(priority = 1)
    public void testInterview() {
        openUrl(ROOT_URL);
        mainPage.clickOnLoans()
                .clickMortgage()
                .clickOnCalculateYourPayments()
                .waitCalculatePage();

        calculatorPage.scrollToPurchase();
        int purchasePriceBefore = calculatorPage.getPurchasePrice();
        calculatorPage.movePurchaseSlider(100, 0);
        int purchasePriceAfter = calculatorPage.getPurchasePrice();

        Assert.assertNotEquals(purchasePriceBefore, purchasePriceAfter);

        calculatorPage.movePurchaseSlider(-100, 0);
        int purchasePrice = calculatorPage.getPurchasePrice();
        while (purchasePrice < PURCHASE_PRICE) {
            calculatorPage.clickOnPrixProprietePlusButton();
            purchasePrice = calculatorPage.getPurchasePrice();
        }
        Assert.assertEquals(purchasePrice, PURCHASE_PRICE, "Incorrect purchace price");

        int downPayment = calculatorPage.getDownPayment();
        while (downPayment < DOWN_PAYMENT) {
            calculatorPage.clickOnDownPaymentPlusButton();
            downPayment = calculatorPage.getDownPayment();
        }
        Assert.assertEquals(downPayment, DOWN_PAYMENT, "Incorrect down payment");

        calculatorPage.clickOnDownPaymentPlusButton();
        calculatorPage.clickOnDropDownAmortizationYears();
        calculatorPage.chooseAmortizationYear15Option();
        calculatorPage.clickOnDropDownPaymentFrequency();
        calculatorPage.choosePaymentFrequencyWeeklyOption();
        calculatorPage.sendTauxInteret("5");
        calculatorPage.clickOnCalculateButton();
        calculatorPage.waitPaymentResult();

        Assert.assertEquals(calculatorPage.getPaymentResult(), "$ 836.75", "Incorrect payment amount");
    }

}
