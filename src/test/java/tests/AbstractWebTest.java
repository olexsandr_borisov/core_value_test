package tests;

import org.openqa.selenium.*;

import java.util.concurrent.TimeUnit;

import ru.yandex.qatools.allure.annotations.Step;
import configs.TestConfig;

public class AbstractWebTest extends TestConfig {

    @Step("Open pages {0}")
    public void openUrl(String url) {
        getDriver().get(url);
        waitUntilRequestsHaveFinished();
    }

    public void waitUntilRequestsHaveFinished() {
        getDriver().manage().timeouts().setScriptTimeout(40, TimeUnit.SECONDS);
        ((JavascriptExecutor) getDriver()).executeAsyncScript(
                "var callback = arguments[arguments.length - 1]; var xhr = new XMLHttpRequest(); xhr.open('POST', '/Ajax_call', true); xhr.onreadystatechange = function() { if (xhr.readyState == 4) {callback(xhr.responseText);}}; xhr.send();");
    }
}
